package me.plugin.survivalplugin.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import me.plugin.survivalplugin.Main;

import java.util.UUID;

public class HomeCommand implements CommandExecutor {
    private final Main plugin;

    public HomeCommand(Main plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            final Player p = (Player) sender;
            final UUID id = p.getUniqueId();

            if (!this.plugin.hasHome(id)) {

                p.sendMessage(ChatColor.RED + "Je hebt nog geen home gezet, doe /sethome om een home te zetten");

            } else {

                this.plugin.addQue(id);

                (new BukkitRunnable() {
                    int delay = 3;

                    public void run() {

                        if (HomeCommand.this.plugin.isQued(id)) {

                            if (this.delay == 0) {

                                p.teleport(HomeCommand.this.plugin.getHome(id));
                                p.sendMessage(ChatColor.GREEN + "Je wordt naar je home geteleporteerd!");

                                HomeCommand.this.plugin.cancelQue(id);
                                cancel();

                            } else if (this.delay == 3) {

                                p.sendMessage(ChatColor.GREEN + "Je wordt over 3 seconden naar je home geteleporteerd");

                            } else if (this.delay == 2) {

                                p.sendMessage(ChatColor.GREEN + "Je wordt over 2 seconden naar je home geteleporteerd");

                            } else if (this.delay == 1) {

                                p.sendMessage(ChatColor.GREEN + "Je wordt over 1 seconde naar je home geteleporteerd");

                            }

                        } else {

                            p.sendMessage(ChatColor.RED
                                    + "Je bewoog of werd aangevallen tijdens het teleporteren naar je home, daarom is het teleporteren gestopt. Doe /home om toch te teleporteren!");

                            cancel();

                        }

                        // is hetzelfde als -1 seconde
                        this.delay--;

                    }

                }).runTaskTimer((Plugin) this.plugin, 0L, 20L);

            }

        }

        return true;

    }

}
