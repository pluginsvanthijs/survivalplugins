package me.plugin.survivalplugin.commands;

import me.plugin.survivalplugin.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class SetHomeCommand implements CommandExecutor, Listener {

    private final Main plugin;

    public SetHomeCommand(Main plugin) {

        this.plugin = plugin;

    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (sender instanceof Player) {

            String customInventoryTitle = ChatColor.DARK_GRAY + "Wil je je home verplaatsen?";
            Player p = (Player) sender;
            UUID id;
            id = p.getUniqueId();
            Location location = p.getLocation();

            Inventory inv = Bukkit.createInventory(null, 45, customInventoryTitle);

            ItemStack yes = new ItemStack(Material.LIME_WOOL);
            ItemMeta Yes = yes.getItemMeta();
            ItemStack no = new ItemStack(Material.RED_WOOL);
            ItemMeta No = no.getItemMeta();
            ItemStack glass = new ItemStack(Material.WHITE_STAINED_GLASS_PANE);
            ItemMeta Glass = glass.getItemMeta();
            ItemStack barrier = new ItemStack(Material.BARRIER);
            ItemMeta Barrier = barrier.getItemMeta();

            Yes.setDisplayName(ChatColor.GREEN + "Ja, ik wil mijn home verplaatsen");
            yes.setItemMeta(Yes);
            No.setDisplayName(ChatColor.RED + "Nee, ik wil mijn home niet verplaatsen");
            no.setItemMeta(No);
            Glass.setDisplayName(ChatColor.WHITE + " ");
            glass.setItemMeta(Glass);
            Barrier.setDisplayName(ChatColor.RED + "Sluit dit menu");
            barrier.setItemMeta(Barrier);

            inv.setItem(12, yes);
            inv.setItem(14, no);
            inv.setItem(27, glass);
            inv.setItem(28, glass);
            inv.setItem(29, glass);
            inv.setItem(30, glass);
            inv.setItem(31, glass);
            inv.setItem(32, glass);
            inv.setItem(33, glass);
            inv.setItem(34, glass);
            inv.setItem(35, glass);
            inv.setItem(40, barrier);

            if (this.plugin.hasHome(id)) {

                p.openInventory(inv);

            } else if (!this.plugin.hasHome(id)) {

                p.sendMessage(ChatColor.GREEN + "Je home is aangemaakt");
                plugin.getFiles().addHome(id, location);
                plugin.addHome(id, location);

            }

        }

        return true;

    }

    @EventHandler
    public void onMenuClick(InventoryClickEvent e) {

        String customInventoryTitle = ChatColor.DARK_GRAY + "Wil je je home verplaatsen?";
        Player player = (Player) e.getWhoClicked();
        UUID id = player.getUniqueId();
        Location location = player.getLocation();

        Inventory inv = Bukkit.createInventory(null, 45, customInventoryTitle);

        ItemStack yes = new ItemStack(Material.LIME_WOOL);
        ItemMeta Yes = yes.getItemMeta();
        ItemStack no = new ItemStack(Material.RED_WOOL);
        ItemMeta No = no.getItemMeta();
        ItemStack glass = new ItemStack(Material.WHITE_STAINED_GLASS_PANE);
        ItemMeta Glass = glass.getItemMeta();
        ItemStack barrier = new ItemStack(Material.BARRIER);
        ItemMeta Barrier = barrier.getItemMeta();

        Yes.setDisplayName(ChatColor.GREEN + "Ja, ik wil mijn home verplaatsen");
        yes.setItemMeta(Yes);
        No.setDisplayName(ChatColor.RED + "Nee, ik wil mijn home niet verplaatsen");
        no.setItemMeta(No);
        Glass.setDisplayName(ChatColor.WHITE + " ");
        glass.setItemMeta(Glass);
        Barrier.setDisplayName(ChatColor.RED + "Sluit dit menu");
        barrier.setItemMeta(Barrier);

        inv.setItem(12, yes);
        inv.setItem(14, no);
        inv.setItem(27, glass);
        inv.setItem(28, glass);
        inv.setItem(29, glass);
        inv.setItem(30, glass);
        inv.setItem(31, glass);
        inv.setItem(32, glass);
        inv.setItem(33, glass);
        inv.setItem(34, glass);
        inv.setItem(35, glass);
        inv.setItem(40, barrier);

        if(!e.getInventory().contains(Material.BARRIER)) {

            return;

        }else {

            if (e.getCurrentItem() == null) {

            }

            if (e.getCurrentItem().getType() == Material.LIME_WOOL) {

                this.plugin.addHome(id, location);
                player.sendMessage(ChatColor.GREEN + "Je home is verplaatst");
                player.closeInventory();

            } else if (e.getCurrentItem().getType() == Material.RED_WOOL) {

                player.sendMessage(ChatColor.RED + "Je home is niet verplaatst");
                player.closeInventory();

            } else if (e.getCurrentItem().getType() == Material.WHITE_STAINED_GLASS_PANE) {

                e.setCancelled(true);

            } else if (e.getCurrentItem().getType() == Material.BARRIER) {

                player.closeInventory();
                e.setCancelled(true);

            }

        }

    }

}
