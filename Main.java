import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import me.plugin.survivalplugin.commands.SetHomeCommand;
import me.plugin.survivalplugin.commands.VanishCommand;
import me.plugin.survivalplugin.events.HomeFiles;
import me.plugin.survivalplugin.events.PlayerListeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Main extends JavaPlugin {

    private HashMap<UUID, Location> homes;

    private HomeFiles files;

    private List<UUID> que;

    public ArrayList<Player> invisible_list = new ArrayList<>();

    public void onEnable() {

        if (!getDataFolder().exists())
            getDataFolder().mkdir();
        this.homes = new HashMap<>();
        this.files = new HomeFiles(this);
        this.que = new ArrayList<>();
        this.files.init();

        getConfig().options().copyDefaults(true);
        saveConfig();

        getCommand("sethome").setExecutor(new SetHomeCommand(this));
        getCommand("home").setExecutor((new HomeCommand(this)));

        getServer().getPluginManager().registerEvents(new PlayerListeners(this), (Plugin) this);
        getServer().getPluginManager().registerEvents(new SetHomeCommand(this), (Plugin) this);

    }

    public void onDisable() {

        this.files.terminate();
        saveConfig();

    }

    public void addHome(UUID id, Location location) {
        this.homes.put(id, location);
    }

    public Location getHome(UUID id) {return this.homes.get(id);}

    public boolean hasHome(UUID id) {
        return this.homes.containsKey(id);
    }

    public HashMap<UUID, Location> getHomes() {
        return this.homes;
    }

    public HomeFiles getFiles() {
        return this.files;
    }

    public void addQue(UUID id) {
        this.que.add(id);
    }

    public void cancelQue(UUID id) {
        this.que.remove(id);
    }

    public boolean isQued(UUID id) {
        return this.que.contains(id);
    }

}
