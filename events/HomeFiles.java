package me.plugin.survivalplugin.events;

import me.plugin.survivalplugin.Main;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class HomeFiles {

    private final Main plugin;

    public HomeFiles(Main plugin) {

        this.plugin = plugin;

    }

    public void init() {

        File homeFiles = new File(plugin.getDataFolder(), "homes.yml");

        if(homeFiles.exists()) {

            YamlConfiguration homeConfig = YamlConfiguration.loadConfiguration(homeFiles);
            for (String s : homeConfig.getKeys(false)) {

                UUID playerId = UUID.fromString(s);
                Location homeLocation = homeConfig.getLocation(s);
                this.plugin.addHome(playerId, homeLocation);

            }

        }

    }

    public void terminate() {

        File homeFiles = new File(plugin.getDataFolder(), "homes.yml");
        if (!homeFiles.exists())

            try {
                homeFiles.createNewFile();

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    public void addHome(UUID id, Location location) {

        File homeFiles = new File(plugin.getDataFolder(), "homes.yml");
        if (!homeFiles.exists())

            try {

                homeFiles.createNewFile();

            } catch (IOException e) {

                e.printStackTrace();

            }

        YamlConfiguration homeConfig = YamlConfiguration.loadConfiguration(homeFiles);
        homeConfig.set(id.toString(), location);

        try {

            homeConfig.save(homeFiles);

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

}
