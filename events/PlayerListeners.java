package me.plugin.survivalplugin.events;

import me.plugin.survivalplugin.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public class PlayerListeners implements Listener {
    private final Main plugin;

    public PlayerListeners(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {

        if (!e.getFrom().getBlock().equals(e.getTo().getBlock())) {
            Player p = e.getPlayer();
            UUID id = p.getUniqueId();

            if (this.plugin.isQued(id))
                this.plugin.cancelQue(id);

        }

    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {

        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            UUID id = p.getUniqueId();

            if (this.plugin.isQued(id))
                this.plugin.cancelQue(id);

        }

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();
        e.setJoinMessage(ChatColor.GRAY + "[" + ChatColor.GREEN + "+" + ChatColor.GRAY + "] " + p.getDisplayName()
                + " is zojuist de server gejoined");

        for(int i = 0; i < plugin.invisible_list.size(); i++) {

            if(p.hasPermission("command.admin.vanish")) {

                p.sendMessage(ChatColor.AQUA + "Er zijn op dit moment " + ChatColor.YELLOW + i + ChatColor.AQUA + " mensen in vanish");

            }else {

                p.hidePlayer(plugin, plugin.invisible_list.get(i));

            }

        }

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player p = e.getPlayer();
        e.setQuitMessage(ChatColor.GRAY + "[" + ChatColor.RED + "-" + ChatColor.GRAY + "] " + p.getDisplayName()
                + " is zojuist de server geleaved");

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        Player p = e.getPlayer();
        String msg = e.getMessage();

        if (p.hasPermission("chat.moderator")) {

            e.setFormat(String.valueOf(String.valueOf(ChatColor.DARK_RED + p.getDisplayName())) + ": " + ChatColor.WHITE
                    + msg);

        } else {

            e.setFormat(
                    String.valueOf(String.valueOf(ChatColor.GRAY + p.getDisplayName())) + ": " + ChatColor.WHITE + msg);

        }

    }

}
